<?php

return [
    'music' => 'musique',
    'biography' => 'biographie',
    'quote' => 'devis',
    'contact' => 'contact',
    'send' => 'envoi',
    'edit' => 'modifier',
    'sitemap' => 'plan',
];