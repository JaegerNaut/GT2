let bulmaCarousel = require('bulma-carousel');
let bulmaCalendar = require('bulma-calendar');
let moment = require('moment');

document.addEventListener('DOMContentLoaded', () => {

    /*===== COLLAPASABLE NAVBAR =====*/
    if (document.querySelector('#app')) {
        const navbar = document.querySelector('.navbar-burger');
        navbar.addEventListener('click', () => {
            navbar.classList.toggle('is-active');
            document.querySelector('.navbar-menu').classList.toggle('is-active');
        });
    }

    /*===== REMOVE ALERT MESSAGE =====*/

    const remove = document.querySelector('.notification .delete');

    if (remove) {
        const notif = remove.parentNode;
        remove.addEventListener('click', () => {
            notif.parentNode.removeChild(notif);
        })
    }

});

/*===== TIMELINE ===== */

(() => {

    'use strict';

    // define variables
    let items = document.querySelectorAll(".timeline li");

    function isElementInViewport(el) {
        let rect = el.getBoundingClientRect();
        return (
            rect.top >= 0 &&
            rect.left >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.right <= (window.innerWidth || document.documentElement.clientWidth)
        );
    }

    function callbackFunc() {
        for (let i = 0; i < items.length; i++) {
            if (isElementInViewport(items[i])) {
                items[i].classList.add("in-view");
            }
        }
    }

    // listen for events
    window.addEventListener("load", callbackFunc);
    window.addEventListener("resize", callbackFunc);
    window.addEventListener("scroll", callbackFunc);

    document.querySelectorAll('iframe').forEach((i) => {
        i.setAttribute('height', 315);
        i.setAttribute('height', 315);
    })

})();

/* ====== CAROUSEL ====== */

const music = document.querySelector('#app .music');

if (music) {
    let carousel = bulmaCarousel.attach('#carousel', {
        slidesToScroll: 1,
        slidesToShow: 1,
        navigation: false,
        pagination: false
    });

    const puremusic = document.querySelector('#puremusic');
    const compoimage = document.querySelector('#compoimage');

    compoimage.addEventListener('click', () => {
        compoimage.classList.add('selected');
        puremusic.classList.remove('selected');
        carousel[0].previous()
    });

    puremusic.addEventListener('click', () => {
        puremusic.classList.add('selected');
        compoimage.classList.remove('selected');
        carousel[0].next()
    });
}

/*===== PROFILE UPLOAD PREVIEW ======*/
const user = document.querySelector('.back .user');

if (user) {
    const picture = document.getElementById('picture');
    picture.addEventListener('change', () => {
        if (picture.files && picture.files[' + (count + 1) + ']) {
            let reader = new FileReader();

            reader.onload = (e) => {
                document.querySelector('.preview').innerHTML = '<img src="' + e.target.result +'" alt="preview"/>'
            };

            reader.readAsDataURL(picture.files[' + (count + 1) + '])
        }
    })
}

/*===== ADDING/SUPPRESSING MUSIC RATIOS FIELDS ======*/

if (document.querySelector('.back .settings')) {
    document.querySelector('.add-type button').addEventListener('click', () => {

        let count = document.querySelectorAll('.ratios .columns').length;

        let div = document.createElement('div');
        div.classList.add('columns');
        div.innerHTML = '' +
            '<div class="column is-half">' +
                '<div class="control">' +
                    '<label for="type">Type de la musique</label>' +
                    '<input type="text" name="music_ratio[' + (count + 1) + '][music_genre]">' +
                '</div>' +
            '</div>' +
            '<div class="column">' +
                '<label for="rate">Taux</label>' +
                '<input type="text" name="music_ratio[' + (count + 1) + '][ratio]">' +
            '</div>' +
            '<div class="column">' +
                '<button class="remove" type="button">&times;</button>' +
            '</div>';

        document.querySelector('.ratios').appendChild(div);
    });

    document.addEventListener('click', (e) => {
        if (e.target.matches('button.remove')) {
            let columns = e.target.closest('.columns');
            columns.parentNode.removeChild(columns)
        }
    })
}


/*===== QUOTE CALENDAR ===== */

if (document.querySelector('.quote.create')) {
    let parameters = JSON.parse(document.querySelector('input.params').value);
    let mr = JSON.parse(document.querySelector('input.mr').value);
    let dates = null;
    if (document.querySelector('.quotes.edit')) {
        dates = JSON.parse(document.querySelector('input.dates').value);
    }

    const calendar = bulmaCalendar.attach('[type="date"]', {
        color: 'black',
        showHeader: false,
        dateFormat: 'DD-MM-YYYY'
    });

    if (dates) {
        calendar[0].value(moment(dates[0]).format('DD-MM-YYYY'));
        calendar[1].value(moment(dates[1]).format('DD-MM-YYYY'));
    }

    calendar[1].value('10/20/2020');

    /*===== SHOW / HIDE CALENDAR =====*/

    let begindate = document.querySelector('input.check-begindate');
    begindate.addEventListener('click',  () => {
        document.querySelector('.div-begindate').classList.toggle('is-hidden');
    });

    let deadline = document.querySelector('input.check-deadline');
    deadline.addEventListener('click',  () => {
        document.querySelector('.div-deadline').classList.toggle('is-hidden');
    });


    document.querySelector('.datetimepicker-clear-button').addEventListener('click', (e) => {
        e.preventDefault()
    });

    /*===== ADD / REMOVE SPECS FROM QUOTE FORM ===== */


    let options = '';
    mr.map( m => {
        options += '<option value="'+ m.music_genre +'-'+ m.ratio +'">'+ m.music_genre +'</option>'
    });

    document.querySelector('#add-spec').addEventListener('click', () => {

        let count = document.querySelectorAll('.fields hr').length;
        let div = document.createElement('div');
        div.classList.add('columns');
        div.innerHTML = 
            '<div class="column is-6">' +
                '<div class="column is-half is-offset-5">' +
                    '<div class="control">' +
                        '<select name="specs[' + count + '][genre]" id="genre" required>' +
                            '<option value="" selected disabled hidden>'+ parameters['locale_' + parameters.locale].pick_music +'</option>'+
                            options +
                        '</select>' +
                    '</div>' +
                    '<div class="control">' +
                        '<label for="minutes">'+ parameters['locale_' + parameters.locale].length +'</label>' +
                        '<div class="length">' +
                            '<input type="number" name="specs[' + count + '][minutes]" placeholder="Minutes" min="0" max="60" required> :' +
                            '<input type="number" name="specs[' + count + '][seconds]" placeholder="'+ parameters['locale_' + parameters.locale].seconds +'" min="0" max="60" required>' +
                        '</div>' +
                    '</div>' +
                    '<div class="control">' +
                        '<label for="deadline">'+ parameters['locale_' + parameters.locale].image +'</label><br />' +
                        '<label class="switch">' +
                            '<input type="checkbox">' +
                            '<span class="slider"></span>' +
                        '</label>' +
                    '</div>' +
                '</div>' +
            '</div>' +
            '<div class="column is-6">' +
                '<div class="control column is-half">' +
                    '<textarea name="specs[' + count + '][description]" placeholder="'+ parameters['locale_' + parameters.locale].details +'" id="" cols="30" rows="6" required></textarea>' +
                    '<div class="actions">' +
                        '<button type="button"> &times; '+ parameters['locale_' + parameters.locale].delete +'</button>' +
                    '</div>' +
                '</div>' +
            '</div>';

        document.querySelector('.fields').appendChild(div);
        document.querySelector('.fields').insertBefore(document.createElement('hr'), div.nextSibling);

    });

    document.addEventListener('click', (e) => {
        let count = document.querySelectorAll('.fields hr').length;
        if (e.target.matches('.columns .actions > button')) {
            if (count > 2) {
                let columns = e.target.closest('.columns');
                let hr = columns.nextSibling;
                columns.parentNode.removeChild(hr);
                columns.parentNode.removeChild(columns);
            }

        }
    })
}


/*===== CREATE / UPDATE / DELETE BIOGRAPHY MODALE ===== */

/* ADD */
let biography = document.querySelector('.back .biography');
if (biography) {

    document.addEventListener('click', (e) => {
        if (e.target.classList.contains('add-date')) {
            document.querySelector('.modal.add-date').classList.add('is-active');
        }

        if(document.querySelector('.modal.is-active')) {

            if (e.target.classList.contains('modal-background') || e.target.classList.contains('delete')) {
                e.target.closest('.modal').classList.remove('is-active')
            }
        }

        if (e.target.classList.contains('edit-date')) {
            const id = e.target.closest('.card').getAttribute('id');
            let xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", '/bio/' + id, false );
            xmlHttp.send( null );
            if (xmlHttp.responseText !== "") {
                const data = JSON.parse(xmlHttp.responseText);
                document.querySelector('.modal.edit-date #id').value = id;
                document.querySelector('.modal.edit-date #date').value = data.date;
                document.querySelector('.modal.edit-date #text_fr').value = data.text_fr;
                document.querySelector('.modal.edit-date #text_en').value = data.text_en;
                document.querySelector('.modal.edit-date').classList.add('is-active');
            }
        }

        if (e.target.classList.contains('delete-date')) {
            document.querySelector('.modal.delete-date #id').value = e.target.closest('.card').getAttribute('id');
            document.querySelector('.modal.delete-date').classList.add('is-active');
        }
    });
}

/*===== CREATE / UPDATE / DELETE MUSIC MODALE ===== */

if (document.querySelector('.back .music')) {
    document.addEventListener('click', (e) => {
        if (e.target.classList.contains('add-music')) {
            document.querySelector('.modal.add-music').classList.add('is-active');
        }

        if(document.querySelector('.modal.is-active')) {

            if (e.target.classList.contains('modal-background') || e.target.classList.contains('delete')) {
                e.target.closest('.modal').classList.remove('is-active')
            }
        }

        if (e.target.classList.contains('edit-music')) {
            const id = e.target.closest('.card').getAttribute('id');
            let xmlHttp = new XMLHttpRequest();
            xmlHttp.open( "GET", '/music/' + id, false );
            xmlHttp.send( null );
            if (xmlHttp.responseText !== "") {
                const data = JSON.parse(xmlHttp.responseText);
                document.querySelector('.modal.edit-music #id').value = id;
                document.querySelector('.modal.edit-music #title').value = data.title;
                document.querySelector('.modal.edit-music #author').value = data.author;
                document.querySelector('.modal.edit-music #year').value = data.year;
                document.querySelector('.modal.edit-music #subtitle').value = data.subtitle;
                document.querySelector('.modal.edit-music #content_fr').value = data.content_fr;
                document.querySelector('.modal.edit-music #content_en').value = data.content_en;
                document.querySelector('.modal.edit-music #iframe').value = data.iframe;
                document.querySelector('.modal.edit-music').classList.add('is-active');
            }
        }

        if (e.target.classList.contains('delete-date')) {
            document.querySelector('.modal.delete-date #id').value = e.target.closest('.card').getAttribute('id');
            document.querySelector('.modal.delete-date').classList.add('is-active');
        }
    });

}

/*====== ADMIN QUOTE DISPLAY / CHANGE STATUS / DESTROY =====*/
if (document.querySelector('.back .quotes.show')) {
    document.addEventListener('click', (e) => {
        if (e.target.classList.contains('modal-background') || e.target.classList.contains('delete')) {
            e.target.closest('.modal').classList.remove('is-active')
        }

        if (e.target.classList.contains('status')) {
            document.querySelector('.modal.status').classList.add('is-active');
        }

        if (e.target.classList.contains('destroy')) {
            document.querySelector('.modal.destroy').classList.add('is-active');
        }
    })
}