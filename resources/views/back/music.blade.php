@extends('layouts.back')

@section('title', 'Musiques composées')

@section('content')
    <div class="container music">
        @if ($errors->any())
            <div class="notification is-danger">
                <button class="delete"></button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('status'))
            <div class="notification is-success">
                <button class="delete"></button>
                {{ session('status') }}
            </div>
        @endif

        <div class="add">
            <a class="add-music">+ Nouvelle musique</a>
        </div>

        <div class="musics">
            <div class="columns">
                @foreach($musics as $music)
                    <div class="column is-3 card" id="{{ $music->id }}">
                        <div class="card-content">
                            <div class="content">
                                <h3 class="harrison">{{ $music->title }}</h3>
                                <p>{{ $music->author }} ({{ $music->year }})</p>
                            </div>
                        </div>
                        <div class="bottom">
                            <button type="button" class="delete-music">Supprimer</button>
                            <button type="button" class="edit-music">Modifier</button>
                        </div>
                    </div>
                @endforeach
            </div>

            <div class="modal add-music">
                <div class="modal-background"></div>
                <div class="modal-card">
                    <section class="modal-card-body">
                        <div class="remove">
                            <button class="delete" aria-label="close"></button>
                        </div>

                        <h3 class="harrison">Nouvelle musique</h3>
                        <form action="{{ route('admin.music.save') }}" method="POST">
                            @csrf
                            <div class="fields">
                                <label for="title">Titre</label>
                                <input type="text" id="title" name="title" required>
                            </div>
                            <div class="fields">
                                <label for="author">Auteur</label>
                                <input type="text" id="author" name="author" required>
                            </div>
                            <div class="fields">
                                <label for="year">Année</label>
                                <input type="text" id="year" placeholder="ex: 2012" name="year" required>
                            </div>
                            <div class="fields">
                                <label for="subtitle">Sous titre</label>
                                <input type="text" id="subtitle" name="subtitle" required>
                            </div>
                            <div class="fields">
                                <label for="content_fr">Texte (français)</label>
                                <textarea name="content_fr" id="content_fr" cols="30" rows="6" required
                                          placeholder="Sauter une ligne pour séparer les informations"></textarea>
                            </div>
                            <div class="fields">
                                <label for="content_en">Texte (anglais)</label>
                                <textarea name="content_en" id="content_en" cols="30" rows="6" required
                                          placeholder="Sauter une ligne pour séparer les informations"></textarea>
                            </div>

                            <div class="fields">
                                <label for="iframe">Vidéo à intégrer</label>
                                <textarea name="iframe" id="iframe" cols="30" rows="6" required></textarea>
                            </div>

                            <div class="bottom">
                                <button type="submit">Enregistrer</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>

            <div class="modal edit-music">
                <div class="modal-background"></div>
                <div class="modal-card">
                    <section class="modal-card-body">
                        <div class="remove">
                            <button class="delete" aria-label="close"></button>
                        </div>

                        <h3 class="harrison">Editer la date</h3>
                        <form action="{{ route('admin.music.update') }}" method="POST">
                            @csrf
                            <input type="hidden" name="id" id="id">
                            <div class="fields">
                                <label for="title">Titre</label>
                                <input type="text" id="title" name="title" required>
                            </div>
                            <div class="fields">
                                <label for="author">Auteur</label>
                                <input type="text" id="author" name="author" required>
                            </div>
                            <div class="fields">
                                <label for="year">Année</label>
                                <input type="text" id="year" placeholder="ex: 2012" name="year" required>
                            </div>
                            <div class="fields">
                                <label for="subtitle">Sous titre</label>
                                <input type="text" id="subtitle" name="subtitle" required>
                            </div>
                            <div class="fields">
                                <label for="content_fr">Texte (français)</label>
                                <textarea name="content_fr" id="content_fr" cols="30" rows="6" required
                                          placeholder="Sauter une ligne pour séparer les informations"></textarea>
                            </div>
                            <div class="fields">
                                <label for="content_en">Texte (anglais)</label>
                                <textarea name="content_en" id="content_en" cols="30" rows="6" required
                                          placeholder="Sauter une ligne pour séparer les informations"></textarea>
                            </div>

                            <div class="fields">
                                <label for="iframe">Vidéo à intégrer</label>
                                <textarea name="iframe" id="iframe" cols="30" rows="6" required></textarea>
                            </div>

                            <div class="bottom">
                                <button type="submit">Enregistrer</button>
                            </div>
                        </form>
                    </section>
                </div>
            </div>
            <div class="modal delete-date">
                <div class="modal-background"></div>
                <div class="modal-card">
                    <section class="modal-card-body">
                        <div class="remove">
                            <button class="delete" aria-label="close"></button>
                        </div>

                        <h3 class="harrison">Supprimer la musique ?</h3>
                        <p>
                            Cette action sera irréversible
                        </p>
                        <div class="bottom">
                            <form action="{{ route('admin.music.destroy') }}" method="POST">
                                @csrf
                                <input type="hidden" id="id" name="id">
                                <button type="submit">Supprimer</button>
                            </form>

                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>
@endsection