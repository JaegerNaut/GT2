@extends('layouts.back')

@section('title', 'Enregistré')

@section('content')
    <div class="container quote sent">
        <div class="columns">
            <div class="column is-6 is-offset-3">
                <h3 class="title">Devis enregistré avec succès</h3>
                <p>Le devis est désormais disponible dans <a href="{{ route('back.quotes') }}">la liste des devis</a>.</p>

                <a href="{{ route('front.quote.pdf', ['slug' => $quote->slug]) }}">Imprimer le devis</a>
            </div>
        </div>
    </div>
@endsection