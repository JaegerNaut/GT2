@extends('layouts.back')

@section('title', 'SGT-' . $quote->slug)

@section('content')
    <div class="container quote show">
        @if (session('status'))
            <div class="notification is-success">
                <button class="delete"></button>
                {{ session('status') }}
            </div>
        @endif

        <div class="notification is-warning">
            <p>
                @lang('quote.warning')<br />
                @lang('quote.warning2')
            </p>
        </div>

        <div class="columns">
            <div class="column is-6">
                <table class="table is-narrow">
                    <thead>
                    <tr>
                        <th>@lang('quote.first_name')</th>
                        <th>@lang('quote.last_name')</th>
                        <th>REFERENCE</th>
                        <th>DATE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td data-label="{{ __('quote.first_name') }}">{{ $quote->first_name }}</td>
                        <td data-label="{{ __('quote.last_name') }}">{{ $quote->last_name }}</td>
                        <td data-label="REFERENCE">DEVIS-SGF-{{ $quote->slug }}</td>
                        <td data-label="DATE">{{ date('d/m/Y', strtotime($quote->created_at)) }}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <table class="table is-hoverable main">
            <thead>
            <tr>
                <th>@lang('quote.selection')</th>
                <th>@lang('quote.start')</th>
                <th>@lang('quote.end')</th>
                <th>@lang('quote.description')</th>
                <th>@lang('quote.pwt')</th>
                <th>Total</th>
            </tr>
            </thead>
            <tbody>
                @php($image = false)
                @php($price = json_decode($quote->price))

                @foreach($quote->specs()->get() as $spec)
                    @php($image = !$spec->image ?: true)
                    <tr>
                        <td data-label="{{ __('quote.selection') }}">
                            {{ explode('-', $spec->genre)[0] }} -
                            @lang('quote.length2') {{ sprintf("%02d", $spec->minutes) }}:{{ sprintf("%02d", $spec->seconds) }}
                            @if($spec->image) | @lang('quote.image2') @endif
                        </td>
                        <td data-label="{{ __('quote.start') }}">{{ $quote->beginning_day ? date(__('quote.df'), strtotime($quote->beginning_day)) : 'Non' }}</td>
                        <td data-label="{{ __('quote.end') }}">{{ $quote->deadline ? date(__('quote.df'), strtotime($quote->deadline)) : __('quote.no') }}</td>
                        <td data-label="{{ __('quote.description') }}">{{ $spec->description }}</td>
                        <td data-label="{{ __('quote.pwt') }}">
                            {{ number_format($price->price, 2) }} {{ $quote->currency }}
                        </td>
                        <td data-label="Total">{{ number_format(($price->price + $price->vat), 2) }} {{ $quote->currency }}</td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        <div class="columns">
            <div class="column details">
                <table class="table is-hoverable rates">
                    <thead>
                    <tr>
                        <th>@lang('quote.detail')</th>
                        <th>@lang('quote.rate')</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>@lang('quote.pps')</td>
                        <td>{{ $config->ratio_billing }} {{ $quote->currency }}</td>
                    </tr>
                    <tr>
                        <td>@lang('quote.vat')</td>
                        <td>{{ $config->tva }} %</td>
                    </tr>
                    @if($image)
                        <tr>
                            <td>@lang('quote.image2')</td>
                            <td>{{ $config->rate_picture }} {{ $quote->currency }}</td>
                        </tr>
                    @endif
                    @if($quote->deadline)
                        <tr>
                            <td>Deadline</td>
                            <td>{{ $config->deadline }} {{ $quote->currency }}</td>
                        </tr>
                    @endif

                    @switch ($price->days)
                        @case ($price->days > 365)
                            @php($deal = 25)
                            @break
                        @case ($price->days <= 365 && $price->days > 270)
                            @php($deal = 20)
                            @break
                        @case ($price->days <= 270 && $price->days > 183)
                            @php($deal = 10)
                            @break
                        @default
                            @php($deal = 0)
                    @endswitch

                    @if($deal > 0)
                            <tr>
                                <td>@lang('quote.bonus_deadline')</td>
                                <td>
                                    <b>-{{ $deal }} %</b>
                                </td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div>
        </div>
        <div class="quote-bottom">
            <a href="{{ route('front.quote.create') }}">@lang('quote.cancel')</a>
            <a href="{{ route('front.quote.edit', ['slug' => $quote->slug]) }}">
                <i class="far fa-edit"></i>
                @lang('quote.edit')
            </a>
            <a href="{{ route('front.quote.send', ['slug' => $quote->slug]) }}">@lang('quote.submit')</a>
        </div>
    </div>
@endsection