@extends('layouts.back')

@section('title', 'Dashboard')

@section('content')
<div class="container dashboard">
    <div class="columns">
        <div class="column is-6">
            <div class="columns">
                <div class="is-6 is-offset-3 card followup">
                    <div class="card-content">
                        <h3 class="title">Suivi des devis</h3>
                        <table class="table">
                            <tbody>
                            <tr>
                                <td>Nouveau devis</td>
                                <td>
                                <span class="tag">
                                  {{ $data['pending'] }}
                                </span>
                                </td>
                                <td><a href="{{ route('admin.quotes', ['selected' => 'pending']) }}">Voir</a></td>
                            </tr>
                            <tr>
                                <td>Devis en cours</td>
                                <td>
                                <span class="tag">
                                  {{ $data['ongoing'] }}
                                </span>
                                </td>
                                <td><a href="{{ route('admin.quotes', ['selected' => 'ongoing']) }}">Voir</a></td>
                            </tr>
                            <tr>
                                <td>Devis terminés</td>
                                <td>
                                <span class="tag">
                                  {{ $data['done'] }}
                                </span>
                                </td>
                                <td><a href="{{ route('admin.quotes', ['selected' => 'done']) }}">Voir</a></td>
                            </tr>
                            </tbody>
                        </table>
                        <div class="bottom">
                            <a href="{{ route('admin.quotes', ['selected' => 'all']) }}">Tout voir</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="column is-6">
            <div class="columns">
                <div class="is-6 is-offset-3 card sumup">
                    <div class="card-content">
                        <h3 class="title">Résumé des ratios</h3>
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>Facturation à la seconde</td>
                                    <td>{{ $qc->ratio_billing }} €</td>
                                </tr>
                                <tr>
                                    <td>TVA</td>
                                    <td>{{ $qc->tva}} %</td>
                                </tr>
                                <tr>
                                    <td>Taux si composition à l'image</td>
                                    <td>{{ $qc->rate_picture }} €</td>
                                </tr>
                                <tr>
                                    <td>Taux si deadline</td>
                                    <td>{{ $qc->deadline }} €</td>
                                </tr>
                                <tr>
                                    <td>Multiplicateur par nouvelle musique</td>
                                    <td>{{ $qc->pnm }} €</td>
                                </tr>
                                @foreach($mr as $m)
                                    <tr>
                                        <td><b>{{ $m->music_genre }}</b></td>
                                        <td><b>{{ $m->ratio }}</b></td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="bottom">
                            <a href="{{ route('admin.settings') }}">Modifier</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection