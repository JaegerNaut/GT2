@extends('layouts.back')

@section('title', 'Modifier ses données personnelles')

@section('content')
    <div class="container biography">
        @if ($errors->any())
            <div class="notification is-danger">
                <button class="delete"></button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('status'))
            <div class="notification is-success">
                <button class="delete"></button>
                {{ session('status') }}
            </div>
        @endif

        <div class="add">
            <a class="add-date">+ Nouvelle date</a>
        </div>

        <div class="dates">
            @php($nbcols = 4)
            <div class="columns">
            @foreach($bio as $b)
                <div class="column is-3 card" id="{{ $b->id }}">
                    <div class="card-content">
                        <div class="content">
                            <h3 class="title">{{ $b->date }}</h3>
                            @foreach(explode("\n", $b->text_fr) as $i => $line)
                                @if ($i < 1)
                                    @if (trim($line))
                                        {!!'<p>' . $line . '</p>' !!}
                                    @endif
                                @elseif ($i === 1)
                                    <p>...</p>
                                @endif
                            @endforeach
                        </div>
                    </div>
                    <div class="bottom">
                        <button type="button" class="delete-date">Supprimer</button>
                        <button type="button" class="edit-date">Modifier</button>
                    </div>
                </div>
            @endforeach
            </div>
        </div>

        <div class="modal add-date">
            <div class="modal-background"></div>
            <div class="modal-card">
                <section class="modal-card-body">
                    <div class="remove">
                        <button class="delete" aria-label="close"></button>
                    </div>

                    <h3 class="harrison">Nouvelle date</h3>
                    <form action="{{ route('admin.bio.save') }}" method="POST">
                        @csrf
                        <div class="fields">
                            <label for="date">Année</label>
                            <input type="text" id="date" name="date" placeholder="ex: 2012" required>
                        </div>
                        <div class="fields">
                            <label for="text_fr">Texte (français)</label>
                            <textarea name="text_fr" id="text_fr" cols="30" rows="6" required placeholder="Sauter une ligne pour séparer deux activités"></textarea>
                        </div>
                        <div class="fields">
                            <label for="text_en">Texte (anglais)</label>
                            <textarea name="text_en" id="text_en" cols="30" rows="6" required placeholder="Sauter une ligne pour séparer deux activités"></textarea>
                        </div>

                        <div class="bottom">
                            <button type="submit">Enregistrer</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>

        <div class="modal edit-date">
            <div class="modal-background"></div>
            <div class="modal-card">
                <section class="modal-card-body">
                    <div class="remove">
                        <button class="delete" aria-label="close"></button>
                    </div>

                    <h3 class="harrison">Editer la date</h3>
                    <form action="{{ route('admin.bio.update') }}" method="POST">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="fields">
                            <label for="date">Année</label>
                            <input type="text" id="date" name="date" placeholder="ex: 2012" required>
                        </div>
                        <div class="fields">
                            <label for="text_fr">Texte (français)</label>
                            <textarea name="text_fr" id="text_fr" cols="30" rows="6" required placeholder="Sauter une ligne pour séparer deux activités"></textarea>
                        </div>
                        <div class="fields">
                            <label for="text_en">Texte (anglais)</label>
                            <textarea name="text_en" id="text_en" cols="30" rows="6" required placeholder="Sauter une ligne pour séparer deux activités"></textarea>
                        </div>

                        <div class="bottom">
                            <button type="submit">Enregistrer</button>
                        </div>
                    </form>
                </section>
            </div>
        </div>
        <div class="modal delete-date">
            <div class="modal-background"></div>
            <div class="modal-card">
                <section class="modal-card-body">
                    <div class="remove">
                        <button class="delete" aria-label="close"></button>
                    </div>

                    <h3 class="harrison">Supprimer la date ?</h3>
                    <p>
                        Cette action sera irréversible
                    </p>
                    <div class="bottom">
                        <form action="{{ route('admin.bio.destroy') }}" method="POST">
                            @csrf
                            <input type="hidden" id="id" name="id">
                            <button type="submit">Supprimer</button>
                        </form>

                    </div>
                </section>
            </div>
        </div>

    </div>
@endsection