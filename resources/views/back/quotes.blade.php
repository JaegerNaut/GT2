@extends('layouts.back')

@section('title', 'Devis')

@section('content')
    <div class="container quotes index">
        @if (session('status'))
            <div class="notification is-success">
                <button class="delete"></button>
                {{ session('status') }}
            </div>
        @endif
        @php($selected = request()->route()->parameters['selected'])
        <h3 class="harrison">
            @switch($selected)
                @case('all')
                    Tous les projets
                    @break
                @case('pending')
                    Projets en attente
                    @break
                @case('ongoing')
                    Projets en cours
                    @break
                @case('done')
                    Projets termines
                    @break
            @endswitch

        </h3>
        <div class="top">
            <a href="{{ route('admin.quotes', ['selected' => 'all']) }}" @if($selected === 'all')class="active"@endif>Tout</a>
            <a href="{{ route('admin.quotes', ['selected' => 'pending']) }}" @if($selected === 'pending')class="active"@endif>En attente</a>
            <a href="{{ route('admin.quotes', ['selected' => 'ongoing']) }}" @if($selected === 'ongoing')class="active"@endif>En cours</a>
            <a href="{{ route('admin.quotes', ['selected' => 'done']) }}" @if($selected === 'done')class="active"@endif>Terminé</a>
        </div>

{{--        {{ dd($quotes) }}--}}

        <table class="table">
            <thead>
            <tr>
                <th>Nom</th>
                <th>Adresse e-mail</th>
                <th>Statut</th>
                <th>Prix estimé</th>
                <th>Date de début</th>
                <th>Deadline</th>
                <th>Date de création</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @foreach($quotes as $quote)
                @php($price = json_decode($quote->price))
                <tr>
                    <td>{{ $quote->first_name }} {{ $quote->last_name }}</td>
                    <td><a href="mailto:{{ $quote->email }}">{{ $quote->email }}</a></td>
                    <td>
                        @switch($quote->status)
                            @case('pending')
                                En attente
                                @break
                            @case('ongoing')
                                En cours
                                @break
                            @case('done')
                                Terminé
                                @break
                        @endswitch
                    </td>
                    <td>{{ number_format($price->price + $price->vat, 2) }} €</td>
                    <td>{{ new DateTime() < new DateTime($quote->beginning_day) ? date("d/m/Y", strtotime($quote->beginning_day))  : 'Non définie' }}</td>
                    <td>{{ $quote->deadline ? date("d/m/Y", strtotime($quote->deadline))  : 'Non définie' }}</td>
                    <td>{{ date("d/m/Y", strtotime($quote->created_at)) }}</td>
                    <td><a href="{{ route('admin.quote', ['slug' => $quote->slug]) }}">Voir</a></td>
                </tr>
            @endforeach
            </tbody>
        </table>

        <div class="column is-4 is-offset-4">
            {{ $quotes->links() }}
        </div>


    </div>
@endsection