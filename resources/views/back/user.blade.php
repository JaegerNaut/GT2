@extends('layouts.back')

@section('title', 'Paramétrage des devis')

@section('content')
    <div class="columns user">
        <form action="{{ route('admin.save') }}" class="column is-half is-offset-3" method="POST" enctype="multipart/form-data">
            @csrf()
            @if ($errors->any())
                <div class="notification is-danger">
                    <button class="delete"></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('status'))
                <div class="notification is-success">
                    <button class="delete"></button>
                    {{ session('status') }}
                </div>
            @endif
            <div class="fields">
                <div class="control">
                    <label for="email">Adresse e-mail</label>
                    <input type="text" id="email" name="email" value="{{ $user->email }}" required />
                </div>
                <div class="control">
                    <div class="preview">
                        @if($user->picture)
                            <img src="{{ $user->picture }}" alt="picture">
                        @endif
                    </div>
                    <div class="file">
                        <label class="file-label">
                            <input class="file-input" id="picture" type="file" name="photo">
                            <span class="file-cta">
                              <span class="file-icon">
                                <i class="fas fa-upload"></i>
                              </span>
                              <span class="file-label">
                                Sélectionner un fichier...
                              </span>
                            </span>
                        </label>
                    </div>
                </div>
                <div class="control">
                    <label for="phone">Numéro de téléphone</label>
                    <input type="text" name="phone" id="phone" value="{{ $user->phone }}" required />
                </div>
                <div class="control">
                    <label for="description_fr">Description (fr)</label>
                    <textarea name="description_fr" id="description_fr" rows="5">{{ $user->description_fr }}</textarea>
                </div>
                <div class="control">
                    <label for="description_en">Description (en)</label>
                    <textarea name="description_en" id="description_en" rows="5">{{ $user->description_en }}</textarea>
                </div>

                <hr>

                <div class="control">
                    <label for="npwd">Nouveau mot de passe</label>
                    <input type="password" name="password" id="npwd" />
                </div>
                <div class="control">
                    <label for="rpwd">Répéter mot de passe</label>
                    <input type="password" name="password_confirmation" id="rpwd" />
                </div>
            </div>
            <div class="actions">
                <button type="submit">Enregistrer</button>
            </div>
        </form>
    </div>
@endsection