@extends('layouts.back')

@section('title', __('quote.new_quote'))

@section('content')
    <div class="container quote create">
        <h3 class="harrison">@lang('quote.new_quote')</h3>
        <form action="{{ route('front.quote.store') }}" method="POST">
            @csrf()
            @if ($errors->any())
                <div class="notification is-danger">
                    <button class="delete"></button>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if (session('status'))
                <div class="notification is-success">
                    <button class="delete"></button>
                    {{ session('status') }}
                </div>
            @endif
            @php
                $settings = [
                    'locale' => app()->getLocale(),
                    'dateFormat' => app()->getLocale() === 'fr' ? 'DD/MM/YYYY' : 'MM/DD/YYYY',
                    'locale_fr' => [
                        'pick_music' => 'Sélectionnez un type de musique',
                        'length' => 'Durée de la composition',
                        'image' => 'Composition à l\'image ?',
                        'seconds' => 'Secondes',
                        'details' => 'Donnez plus de détails',
                        'delete' => 'Supprimer',
                    ],
                    'locale_en' => [
                        'pick_music' => 'Pick a type of music',
                        'length' => 'Composition\'s length',
                        'image' => 'Composed for the image?',
                        'seconds' => 'Seconds',
                        'details' => 'Provide more details',
                        'delete' => 'Remove',
                    ]
                ]
            @endphp
            <input class="mr" type="hidden" value="{{ json_encode($types) }}">
            <input class="params" type="hidden" value="{{ json_encode($settings) }}">
            <div class="fields">
                <div class="columns">
                    <div class="column is-6">
                        <div class="column is-half is-offset-5">
                            <div class="control">
                                <label for="last_name">@lang('quote.last_name')</label>
                                <input type="text" id="last_name" name="last_name" placeholder="@lang('quote.last_name')" value="{{ old('last_name') }}" required />
                            </div>
                            <div class="control">
                                <label for="first_name">@lang('quote.first_name')</label>
                                <input type="text" id="first_name" name="first_name" placeholder="@lang('quote.first_name')" value="{{ old('first_name') }}" required />
                            </div>
                            <div class="control">
                                <label for="email">@lang('quote.email')</label>
                                <input type="email" id="email" name="email" placeholder="@lang('quote.email')" value="{{ old('email') }}" required />
                            </div>
                        </div>

                    </div>
                    <div class="column is-6">
                        <div class="column is-half">
                            <div class="control">
                                <label for="currency">@lang('quote.currency')</label>
                                <select name="currency" id="currency">
                                    <option value="€">€</option>
                                    <option value="CHF">CHF</option>
                                    <option value="£">£</option>
                                    <option value="$">$</option>
                                </select>
                            </div>
                            <div class="field">
                                <label>@lang('quote.beginning_date')</label>
                                <div class="columns">
                                    <div class="column is-3">
                                        <label class="switch">
                                            <input type="checkbox" class="check-begindate">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                    <div class="column div-begindate is-hidden">
                                        <input type="date" name="beginning_day">
                                    </div>
                                </div>
                            </div>
                            <div class="field">
                                <label>@lang('quote.deadline')</label>
                                <div class="columns">
                                    <div class="column is-3">
                                        <label class="switch">
                                            <input type="checkbox" class="check-deadline">
                                            <span class="slider"></span>
                                        </label>
                                    </div>
                                    <div class="column div-deadline is-hidden">
                                        <input type="date" name="deadline">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <hr />

                <div class="columns">
                    <div class="column is-6">
                        <div class="column is-half is-offset-5">
                            <div class="control">
                                <select name="specs[0][genre]" id="genre" required>
                                    <option value="" selected disabled hidden>@lang('quote.pick_music')</option>
                                    @foreach($types as $type)
                                        <option value="{{ $type->music_genre }}-{{ $type->ratio }}">{{ $type->music_genre }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="control">
                                <label for="minutes">@lang('quote.length')</label>
                                <div class="length">
                                    <input type="number" name="specs[0][minutes]" placeholder="Minutes" min="0" max="60" required> :
                                    <input type="number" name="specs[0][seconds]" placeholder="{{ __('quote.seconds') }}" min="0" max="60" required>
                                </div>
                            </div>
                            <div class="control">
                                <label for="deadline">@lang('quote.image')</label><br />
                                <label class="switch">
                                    <input id="image0" type="checkbox" name="specs[0][image]">
                                    <span class="slider"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="column is-6">
                        <div class="control column is-half">
                            <textarea name="specs[0][description]" required placeholder="{{ __('quote.details') }}" id="" cols="30" rows="6"></textarea>

                            <div class="actions">
                                <button type="button"> &times; @lang('quote.delete')</button>
                            </div>
                        </div>
                    </div>
                </div>

                <hr />
            </div>


            <div class="column is-two-thirds is-offset-4 quote-bottom">
                <button type="button" id="add-spec">+ @lang('quote.add')</button>
                <button type="submit">@lang('quote.next') →</button>
            </div>
        </form>
    </div>
@endsection