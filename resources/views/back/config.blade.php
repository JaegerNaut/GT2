@extends('layouts.back')

@section('title', 'Paramétrage des devis')

@section('content')
    <div class="columns settings">
        <div class="column is-6">
            <h3 class="harrison">Ratios</h3>
            <form action="{{ route('admin.update') }}" class="column is-half is-offset-3" method="POST">
                @csrf()
                @if ($errors->any())
                    <div class="notification is-danger">
                        <button class="delete"></button>
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                @if (session('status'))
                    <div class="notification is-success">
                        <button class="delete"></button>
                        {{ session('status') }}
                    </div>
                @endif

                <div class="fields">
                    <div class="control">
                        <label for="ratio_billing">Prix de la musique à la seconde</label>
                        <input type="text" name="ratio_billing" id="ratio_billing" value="{{ $config->ratio_billing }}" required />
                    </div>
                    <div class="control">
                        <label for="tva">TVA</label>
                        <input type="text" name="tva" id="tva" value="{{ $config->tva }}" required />
                    </div>
                    <div class="control">
                        <label for="rate_picture">Taux pour composition à l'image</label>
                        <input type="text" name="rate_picture" id="rate_picture" value="{{ $config->rate_picture }}" required />
                    </div>
                    <div class="control">
                        <label for="deadline">Bonus si deadline</label>
                        <input type="text" name="deadline" id="deadline" value="{{ $config->deadline }}" required />
                    </div>
                    <div class="control">
                        <label for="pnm">Bonus par musique supplémentaire</label>
                        <input type="text" name="pnm" id="pnm" value="{{ $config->pnm }}" required />
                    </div>
                    <div class="ratios">
                        @foreach($mr as $m)
                        <div class="columns">
                            <div class="column is-half">
                                <div class="control">
                                    <label for="type">Type de la musique</label>
                                    <input type="text" name="music_ratio[0][music_genre]" value="{{ $m->music_genre }}">
                                </div>
                            </div>
                            <div class="column">
                                <label for="rate">Taux</label>
                                <input type="text" name="music_ratio[0][ratio]" value="{{ $m->ratio }}">
                            </div>
                            <div class="column">
                                <button class="remove" type="button">&times;</button>
                            </div>
                        </div>
                        @endforeach
                    </div>

                    <div class="add-type">
                        <button type="button">+ Nouveau type</button>
                    </div>
                    <div class="actions">
                        <button type="submit">Enregistrer</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="column is-6">
            <h3 class="harrison">Simulation</h3>
            <div class="columns">
                <div class="column is-half is-offset-3">
                    <div class="card simulation">
                        <div class="card-content">
                            <div class="content">
                                @foreach($mr as $ratio)
                                    @php
                                        // 60s + 60s + 60s + 30 = 3mn30 = 210s
                                        // Multiplication by seconds of music
                                        $total = $config->ratio_billing * 210;
                                        // Adding the music genre ratio on the top of it
                                        $total = $total * $ratio->ratio;
                                        // Adding the VAT on the top of it
                                        $total = (($total / 100) * $ratio->tva) + $total;
                                        $total_deadline = $total * (float) $config->deadline;
                                        $total_rp = $total * $config->rate_picture;
                                        $total_both = $total_deadline * (float) $config->deadline;
                                    @endphp
                                    <p>Prix estimé pour une musique de {{ $ratio->music_genre }} d'une durée de 3:30 :</p>

                                    <table class="table is-narrow">
                                        <thead>
                                        <tr>
                                            <th>Prix</th>
                                            <th>Prix <i class="fas fa-clock" aria-hidden="true"></i></th>
                                            <th>Prix <i class="fas fa-image" aria-hidden="true"></i></th>
                                            <th>
                                                Prix <i class="fas fa-image" aria-hidden="true"></i> +
                                                <i class="fas fa-clock" aria-hidden="true"></i>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $total }} €</td>
                                                <td>{{ $total_deadline }} €</td>
                                                <td>{{ $total_rp }} €</td>
                                                <td>{{ $total_both }} €</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
@endsection