@extends('layouts.back')

@section('title', 'SGT-' . $quote->slug)

@section('content')
<div class="container quotes show">
    @if (session('status'))
        <div class="notification is-success">
            <button class="delete"></button>
            {{ session('status') }}
        </div>
    @endif
    <h3 class="harrison">SGT-{{ $quote->slug }}</h3>

    <table class="table column is-4">
        <thead>
            <tr>
                <th>REFERENCE</th>
                <th>DATE</th>
                <th>ÉTAT</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>SGT-{{ $quote->slug }}</td>
                <td>{{ date('d/m/Y', strtotime($quote->updated_at)) }}</td>
                <td>
                    @switch($quote->status)
                        @case('pending')
                        En attente
                        @break
                        @case('ongoing')
                        En cours
                        @break
                        @case('done')
                        Terminé
                        @break
                    @endswitch
                </td>
            </tr>
        </tbody>
    </table>

    <table class="table">
        <thead>
            <tr>
                <th>Sélection</th>
                <th>Début de travail demandé</th>
                <th>Deadline</th>
                <th>Informations complémentaires</th>
                <th>Prix hors taxe</th>
                <th>Total</th>
            </tr>
        </thead>
        <tbody>
            @php($price = json_decode($quote->price))
            @foreach($quote->specs()->get() as $spec)
                <tr>
                    <td>
                        {{ explode('-', $spec->genre)[0] }} -
                        @lang('quote.length2') {{ sprintf("%02d", $spec->minutes) }}:{{ sprintf("%02d", $spec->seconds) }}
                        @if($spec->image) | @lang('quote.image2') @endif
                    </td>
                    <td>{{ $quote->beginning_day ? date(__('quote.df'), strtotime($quote->beginning_day)) : 'Non' }}</td>
                    <td>{{ $quote->deadline ? date(__('quote.df'), strtotime($quote->deadline)) : __('quote.no') }}</td>
                    <td>{{ $spec->description }}</td>
                    <td>
                        {{ number_format($price->price, 2) }} {{ $quote->currency }}
                    </td>
                    <td>{{ number_format(($price->price + $price->vat), 2) }} {{ $quote->currency }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>

    <div class="bottom">
        <a href="{{ route('admin.quotes', ['selected' => 'all']) }}">← Retour</a>
        @if($quote->status === 'pending')
            <a class="status">Commencer le projet</a>
        @elseif($quote->status === 'ongoing')
            <a class="status">Terminer le projet</a>
        @endif
        <a class="destroy">Supprimer le devis</a>
    </div>

    <div class="modal status">
        <div class="modal-background"></div>
        <div class="modal-card">
            <section class="modal-card-body">
                <div class="remove">
                    <button class="delete" aria-label="close"></button>
                </div>

                <h3 class="harrison">
                    @if($quote->status === 'pending')
                        Commencer le projet ?
                    @elseif($quote->status === 'ongoing')
                        Terminer le projet ?
                    @endif
                </h3>
                <p>
                    Cette action sera irréversible
                </p>
                <div class="bottom">
                    @if($quote->status === 'pending')
                        <a href="{{ route('admin.quote.change', ['id' => $quote->id, 'status' => 'ongoing']) }}" class="status">Commencer le projet</a>
                    @elseif($quote->status === 'ongoing')
                        <a href="{{ route('admin.quote.change', ['id' => $quote->id, 'status' => 'done']) }}">Terminer le projet</a>
                    @endif
                </div>
            </section>
        </div>
    </div>

    <div class="modal destroy">
        <div class="modal-background"></div>
        <div class="modal-card">
            <section class="modal-card-body">
                <div class="remove">
                    <button class="delete" aria-label="close"></button>
                </div>

                <h3 class="harrison">Supprimer le devis ?</h3>
                <p>
                    Cette action sera irréversible
                </p>

                <div class="bottom">
                    <a href="{{ route('admin.quote.delete', ['id' => $quote->id]) }}" class="status">Supprimer le devis</a>
                </div>

            </section>
        </div>
    </div>
</div>
@endsection
