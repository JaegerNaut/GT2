<p>Hey salut,</p>

<p>Un visiteur a laissé le message suivant :</p>

<p><b>Nom :</b> {{ $contact['name'] }}</p>

<p><b>Subject :</b> {{ $contact['subject'] }}</p>

<p><b>E-mail :</b> {{ $contact['email'] }}</p>

<p><b>Message:</b></p>

{!! nl2br($contact['message']) !!}


<p>Cordialement,</p>

<p>Le site web.</p>