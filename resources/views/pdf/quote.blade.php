<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">

    <style>
        img {
            width: 100px;
        }

        h1 {
            text-align: center;
            text-transform: uppercase;
        }

        .table-ref {
            margin-top: 20px;
            margin-bottom: 50px;
        }

        table {
            border-collapse: collapse;
        }

        table.main th, table.main td {
            border-bottom: solid 1px #ddd;
        }

        .main {
            margin-bottom: 50px;
        }

        .pull-right {
            float: right;
        }
    </style>
</head>
<body>
<div class="columns">
    <img src="{{ public_path() . '/img/logo.png' }}" alt="">
    <h1>@lang('quote.quote')</h1>
    <div class="table-ref">
        <table class="table is-narrow">
            <thead>
            <tr>
                <th>@lang('quote.first_name')</th>
                <th>@lang('quote.last_name')</th>
                <th>REFERENCE</th>
                <th>DATE</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>{{ $quote->first_name }}</td>
                <td>{{ $quote->last_name }}</td>
                <td>DEVIS-SGF-{{ $quote->slug }}</td>
                <td>{{ date('d/m/Y', strtotime($quote->created_at)) }}</td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
<table class="table is-hoverable main">
    <thead>
    <tr>
        <th>@lang('quote.selection')</th>
        <th>@lang('quote.start')</th>
        <th>@lang('quote.end')</th>
        <th>@lang('quote.description')</th>
        <th>@lang('quote.pwt')</th>
        <th>Total</th>
    </tr>
    </thead>
    <tbody>
    @php($image = false)
    @php($price = json_decode($quote->price))

    @foreach($quote->specs()->get() as $spec)
        @php($image = !$spec->image ?: true)
        <tr>
            <td>
                {{ explode('-', $spec->genre)[0] }} -
                @lang('quote.length2') {{ sprintf("%02d", $spec->minutes) }}:{{ sprintf("%02d", $spec->seconds) }}
                @if($spec->image) | @lang('quote.image2') @endif
            </td>
            <td>{{ $quote->beginning_day ? date(__('quote.df'), strtotime($quote->beginning_day)) : 'Non' }}</td>
            <td>{{ $quote->deadline ? date(__('quote.df'), strtotime($quote->deadline)) : __('quote.no') }}</td>
            <td>{{ $spec->description }}</td>
            <td>
                {{ number_format($price->price, 2) }} {{ $quote->currency }}
            </td>
            <td>{{ number_format(($price->price + $price->vat), 2) }} {{ $quote->currency }}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<div class="columns pull-right">
    <table class="table ">
        <thead>
            <tr>
                <th>@lang('quote.detail')</th>
                <th>@lang('quote.rate')</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>@lang('quote.pps')</td>
                <td>{{ $config->ratio_billing }} {{ $quote->currency }}</td>
            </tr>
            <tr>
                <td>@lang('quote.vat')</td>
                <td>{{ $config->tva }} %</td>
            </tr>
            @if($image)
                <tr>
                    <td>@lang('quote.image2')</td>
                    <td>{{ $config->rate_picture }} {{ $quote->currency }}</td>
                </tr>
            @endif
            @if($quote->deadline)
                <tr>
                    <td>Deadline</td>
                    <td>{{ $config->deadline }} {{ $quote->currency }}</td>
                </tr>
            @endif

        @switch ($price->days)
            @case ($price->days > 365)
            @php($deal = 25)
            @break
            @case ($price->days <= 365 && $price->days > 270)
            @php($deal = 20)
            @break
            @case ($price->days <= 270 && $price->days > 183)
            @php($deal = 10)
            @break
            @default
            @php($deal = 0)
        @endswitch

        @if($deal > 0)
            <tr>
                <td>@lang('quote.bonus_deadline')</td>
                <td>
                    <b>-{{ $deal }} %</b>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
</div>
</body>
</html>