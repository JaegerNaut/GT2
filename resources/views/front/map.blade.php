@extends('layouts.front')

@section('title', __('pages.sitemap'))

@section('content')
    <div class="container">
        <h3 class="harrison">@lang('pages.sitemap')</h3>

        <ul>
            <li>
                <a href="{{ route('home') }}">@lang('pages.landing')</a>
            </li>
            <li>
                <a href="{{ route('front.music') }}">@lang('pages.musics')</a>
            </li>
            <li>
                <a href="{{ route('front.bio') }}">@lang('pages.biography')</a>
            </li>
            <li>
                <a href="{{ route('front.contact') }}">Contact</a>
            </li>
            <li>
                <a href="{{ route('legal') }}">@lang('pages.lm')</a>
            </li>
        </ul>
    </div>

@endsection