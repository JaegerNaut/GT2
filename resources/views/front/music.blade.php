@extends('layouts.front')

@section('title', __('pages.musics'))

@section('content')
    <div class="music">
        <div class="columns">
            <div id="compoimage" class="column bullet is-offset-3-widescreen is-offset-2 is-2-widescreen is-3 selected">
                <span>@lang('pages.compoimage')</span>
            </div>
            <div id="puremusic" class="column bullet is-offset-3-widescreen is-offset-2 is-2-widescreen is-3">
                <span>@lang('pages.puremusic')</span>
            </div>
        </div>


        <section>
            <div id="carousel" class="carousel">
                <div class="item-1" data-index="0">
                    <div class="container">
                        <h3 class="harrison">@lang('pages.compoimage2')</h3>
                        @foreach($musics as $music)
                            <div class="columns image">
                                <div class="column is-6">
                                    {!! $music->iframe !!}
                                </div>
                                <div class="is-6 description">
                                    <p class="image-title">{{ $music->title }}</p>
                                    <p class="image-author">{{ $music->author }} - {{ $music->year }}</p>
                                    <p class="image-subtitle">{{ $music->subtitle }}</p>
                                    <div class="image-content">
                                        @foreach(explode("\n", $music['content_'. app()->getLocale()]) as $line)
                                            @if (trim($line))
                                                {!!'<p>' . $line . '</p>' !!}
                                            @endif
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="item-2" data-index="1">
                    <div class="container pure">
                        <h3 class="harrison">@lang('pages.puremusic')</h3>
                        <iframe width="100%" height="450" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/playlists/172592426&color=%23000000&auto_play=false&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true"></iframe>                    </div>
                </div>
            </div>
        </section>
    </div>

@endsection