@extends('layouts.front')

@section('title', __('pages.biography'))


@section('content')
<div class="biography">
    <div class="columns">
        <div class="column is-4 is-offset-2">
            <img src="{{ $user->picture }}" alt="geoffrey">
        </div>
        <div class="column is-4">
            <h3 class="harrison">Geoffrey Turpin</h3>
            @foreach(explode("\n", $user['description_' . app()->getLocale()]) as $p)
                @if(trim($p))
                    {!! '<p>' . $p . '</p>' !!}
                @endif
            @endforeach
        </div>
    </div>

    <section class="timeline">

        <div class="columns">
            <div class="column is-offset-2">
                <h3 class="harrison">@lang('pages.journey')</h3>
            </div>
        </div>

        <ul>
            <li>
                <div>
                    <time>@lang('pages.soon')</time>
                    <p>
                        @lang('pages.soon_desc')
                    </p>
                </div>
            </li>

            @foreach($bio as $b)
            <li>
                <div>
                    <time>{{ $b->date }}</time>
                    @foreach(explode("\n", $b['text_'. app()->getLocale()]) as $line)
                        @if (trim($line))
                            {!!'<p>' . $line . '</p>' !!}
                        @endif
                    @endforeach
                </div>
            </li>
            @endforeach
        </ul>
    </section>

</div>
@endsection