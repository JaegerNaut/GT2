<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="UTF8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Compositeur, musicien et artiste">
    <meta property="og:site_name" content="Geoffrey Turpin" />
    <meta property="og:url" content="https://www.geoffreyturpin.fr/" />
    <meta name="geo.region" content="FR" />
    <meta name="geo.placename" content="Lyon" />
    <title>Geoffrey Turpin | @lang('pages.landing')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
    <div class="landing">
        <div class="columns">
            <div class="logo column is-gapless is-3 is-offset-8-widescreen">
                <img src="{{ asset('img/logo.png') }}" alt="">
            </div>
        </div>
        <div class="columns">
            <div class="links column is-gapless is-5-widescreen is-offset-6-desktop">
                <div class="link">
                    <a href="{{ route('front.bio') }}">@lang('pages.discover')</a>
                </div>
                <div class="link">
                    <a href="{{ route('front.music') }}">@lang('pages.listen')</a>
                </div>
            </div>
        </div>

    </div>
    @include('front.partials._footer')
</body>

</html>