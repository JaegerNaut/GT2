<footer>
    <div class="columns">
        <div class="column is-2">
            <p>
                {{ $user->phone }}
                <br />
                {{ $user->email }}
            </p>
        </div>
        <div class="column is-2-desktop is-offset-4 is-offset-6-widescreen">
            <a href="{{ route('legal') }}">@lang('pages.lm')</a>
        </div>
        <div class="column is-1-widescreen is-2-desktop">
            <a href="{{ route('sitemap') }}">@lang('pages.sitemap')</a>
        </div>
        <div class="column is-1">
            <a href="/locale/{{ session()->get('locale') == 'en' ? 'fr' : 'en' }}">{{ session()->get('locale') == 'en' ? 'Français' : 'English' }}</a>
        </div>
    </div>
</footer>
