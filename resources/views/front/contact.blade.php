@extends('layouts.front')

@section('title', 'Contact')

@section('content')
<div class="contact container">
    @if ($errors->any())
        <div class="notification is-danger">
            <button class="delete"></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    @if (session('status'))
        <div class="notification is-success">
            <button class="delete"></button>
            {{ session('status') }}
        </div>
    @endif
    <div class="columns">
        <div class="column is-6">
            <h3 class="harrison">Geoffrey Turpin</h3>
            <p>+33 6 72 44 40 45</p>
            <p>turpin-geoffrey@live.fr</p>
        </div>

        <div class="column is-6">
            <h3 class="harrison">Contact</h3>
            <form action="{{ route('front.send') }}" method="POST">
                @csrf()
                <input type="hidden" name="recaptcha" id="recaptcha">
                <div class="fields">
                    <div class="control">
                        <input type="text" name="name" placeholder="@lang('pages.name')" value="{{ old('name') }}" required />
                    </div>
                    <div class="control">
                        <input type="text" name="email" placeholder="@lang('pages.email')" value="{{ old('email') }}" required />
                    </div>
                    <div class="control">
                        <input type="text" name="subject" placeholder="@lang('pages.subject')" value="{{ old('subject') }}" required />
                    </div>
                    <textarea id="" cols="30" name="message" placeholder="@lang('pages.message')" rows="5">{{ old('message') }}</textarea>
                </div>
                <div class="actions">
                    <button type="submit">@lang('pages.send')</button>
                </div>
            </form>
        </div>
    </div>
</div>
<script src="//www.google.com/recaptcha/api.js?render={{ env('RECAPTCHA_PUBLIC') }}"></script>
<script>
    grecaptcha.ready(function() {
        grecaptcha.execute("{{ env('RECAPTCHA_PUBLIC') }}", {action: 'contact'})
        .then(function(token) {
            if (token) {
                document.getElementById('recaptcha').value = token;
            }
        })
    })
</script>
@endsection
