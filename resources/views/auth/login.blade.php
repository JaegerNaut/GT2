@extends('layouts.auth')

@section('title', 'Connexion')

@section('content')
<section class="login column is-4">
    <h3 class="harrison">Connexion</h3>
    @if ($errors->any())
        <div class="notification is-danger">
            <button class="delete"></button>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form method="POST" class="column" action="{{ route('login') }}">
        @csrf
        <div class="fields">
            <div class="control">
                <input type="email" name="email" placeholder="Email" value="{{ old('email') }}" required />
            </div>
            <div class="control">
                <input type="password" name="password" placeholder="Mot de passe" value="{{ old('password') }}" required />
            </div>
        </div>
        <div class="actions">
            <button type="submit">Connexion</button>
        </div>

        <a class="btn btn-link" href="{{ route('password.request') }}">
            Mot de passe oublié
        </a>
    </form>
</section>
@endsection
