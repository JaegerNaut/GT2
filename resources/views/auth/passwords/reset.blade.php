@extends('layouts.auth')

@section('content')
    <section class="login column is-4">
        <h3 class="harrison">Reinitialiser le mot de passe</h3>
        @if ($errors->any())
            <div class="notification is-danger">
                <button class="delete"></button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if (session('status'))
            <div class="notification is-success">
                <button class="delete"></button>
                {{ session('status') }}
            </div>
        @endif
        <form method="POST" class="column" action="{{ route('password.update') }}">
            @csrf
            <input type="hidden" name="token" value="{{ $token }}">
            <div class="fields">
                <div class="control">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" value="{{ $email ?? old('email') }}" required autofocus>
                </div>
            </div>
            <div class="fields">
                <div class="control">
                    <label for="password">Nouveau mot de passe</label>
                    <input type="password" id="password" name="password" required>
                </div>
            </div>
            <div class="fields">
                <div class="control">
                    <label for="password_confirmation">Confirmer le mot de passe</label>
                    <input type="password" id="password_confirmation" name="password_confirmation" required>
                </div>
            </div>
            <div class="actions">
                <button type="submit">Réinitialiser le mot de passe</button>
            </div>
        </form>
    </section>
@endsection
