<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Geoffrey Turpin | @yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body class="auth">
@yield('content')
</body>
</html>