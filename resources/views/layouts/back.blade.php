<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Administration | @yield('title')</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body class="back">
    <div class="columns">
        <div class="column is-half is-offset-one-quarter">
            <h3 class="harrison">Administration</h3>
        </div>
        <div class="column side-links">
            <div class="admin-link">
                <a href="{{ route('home') }}">
                    <i class="fas fa-long-arrow-alt-left"></i>
                    Retour au site
                </a>
            </div>
            <div class="admin-link">
                <a href="{{ route('admin.logout') }}">
                    <i class="fas fa-sign-out-alt"></i>
                    Déconnexion
                </a>
            </div>
        </div>
    </div>
    <div class="tabs is-centered">
        <ul>
            <li class="{{ Route::currentRouteName() === 'admin.dashboard' ? 'is-active': '' }}">
                <a href="{{ route('admin.dashboard') }}">
                    <span class="icon is-small"><i class="fas fa-home"></i></span>
                    <span>Dashboard</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() === 'admin.quotes' || Route::currentRouteName() === 'admin.quote' ? 'is-active': '' }}">
                <a href="{{ route('admin.quotes', ['selected' => 'all']) }}">
                    <span class="icon is-small"><i class="fas fa-receipt"></i></span>
                    <span>Voir les devis</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() === 'admin.settings' ? 'is-active': '' }}">
                <a href="{{ route('admin.settings') }}">
                    <span class="icon is-small"><i class="fas fa-cogs"></i></span>
                    <span>Ratios & simulation</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() === 'front.quote.create' ? 'is-active' : '' }}">
                <a href="{{ route('front.quote.create') }}">
                    <span class="icon is-small"><i class="fas fa-file-invoice"></i></span>
                    <span>Nouveau devis</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() === 'admin.bio' ? 'is-active': '' }}">
                <a href="{{ route('admin.bio') }}">
                    <span class="icon is-small"><i class="far fa-clock"></i></span>
                    <span>Biographie</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() === 'admin.music' ? 'is-active': '' }}">
                <a href="{{ route('admin.music') }}">
                    <span class="icon is-small"><i class="fas fa-music"></i></span>
                    <span>Musiques</span>
                </a>
            </li>
            <li class="{{ Route::currentRouteName() === 'admin.user' ? 'is-active': '' }}">
                <a href="{{ route('admin.user') }}">
                    <span class="icon is-small"><i class="fas fa-user"></i></span>
                    <span>Informations utilisateur</span>
                </a>
            </li>
        </ul>
    </div>
    @yield('content')
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>