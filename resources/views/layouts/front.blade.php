<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="apple-touch-icon" sizes="57x57" href="/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/manifest.json">

    <title>
        Geoffrey Turpin | @yield('title')
    </title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
    <div id="app">
        <nav class="navbar" role="navigation" aria-label="main navigation">
            <div class="navbar-brand">
                <a class="navbar-item" href="{{ route('home') }}">
                    <img src="{{ asset('/img/logo.png') }}" alt="home">
                </a>

                <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
            </div>

            <div class="navbar-menu">
                <div class="navbar-start">
                    <div class="item {{ strpos(Route::currentRouteName(), 'bio') ? 'current' : '' }}">
                        <a href="{{ route('front.bio') }}" class="navbar-item">
                            @lang('layout.biography')
                        </a>
                    </div>
                    <div class="item {{ strpos(Route::currentRouteName(), 'music') ? 'current' : '' }}">
                        <a href="{{ route('front.music') }}" class="navbar-item">
                            @lang('layout.musics')
                        </a>
                    </div>
                    <div class="item {{ strpos(Route::currentRouteName(), 'contact') ? 'current' : '' }}">
                        <a href="{{ route('front.contact') }}" class="navbar-item">
                            Contact
                        </a>
                    </div>
                </div>

                <div class="navbar-end">
                    <div class="navbar-item">
                        <a href="#">
                            <img src="{{ asset('/img/facebook.png') }}" alt="fb">
                        </a>
                        <a href="#">
                            <img src="{{ asset('/img/youtube.png') }}" alt="fb">
                        </a>
                        <a href="#">
                            <img src="{{ asset('/img/linkedin.png') }}" alt="fb">
                        </a>
                    </div>
                </div>
            </div>
        </nav>
        <hr />
        <main class="py-4">
            @yield('content')
        </main>
        <hr />
        @include('front.partials._footer')
    </div>
    <script src="{{ asset('js/app.js') }}" defer></script>
</body>
</html>
