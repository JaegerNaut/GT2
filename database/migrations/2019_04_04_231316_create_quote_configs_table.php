<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuoteConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quote_configs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ratio_billing');
            $table->string('tva');
            $table->string('rate_picture');
            $table->string('deadline');
            // Per new music
            $table->string('pnm');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('quote_configs');
    }
}
