<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * @throws Exception
     */
    public function run()
    {

        DB::table('users')->insert([
            'email' => 'jlevarato@pm.me',
            'password' => Hash::make(env('USER_PASSWORD'))
        ]);
/*
        DB::table('quote_configs')->insert([
            'ratio_billing' => '1.00',
            'tva' => '1.00',
            'rate_picture' => '1.00',
            'deadline' => '1.00',
            'pnm' => '1.00',
        ]);

        DB::table('music_ratios')->insert(
            [
                'music_genre' => 'RnB',
                'ratio' => '1.00'
            ],
            [
                'music_genre' => 'Synthwave',
                'ratio' => '1.00'
            ],
            [
                'music_genre' => 'Jazz',
                'ratio' => '1.00'
            ]
        );
*/


    }
}
