<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\MusicRatio;
use App\QuoteConfig;


class QuoteTest extends TestCase
{
    /**
     * Test quote settings creation, check what it contains
     * @test
     *
    */
    public function QuoteConfigCreationTest()
    {
        $config = new QuoteConfig([
            'ratio_billing' => '1.00',
            'tva' => '2.00',
            'rate_picture' => '3.00',
            'deadline_pnm' => '4.00',
            'pnm' => '5.00',
        ]);

        $config->musicRatios()->createMany([
            [
                'music_genre' => 'rnb',
                'ratio' => '3.00',
                'quote_config_id' => 3
            ],
            [
                'music_genre' => 'musique classique',
                'ratio' => '4.00',
                'quote_config_id' => 3
            ],
            [
                'music_genre' => 'synthwave',
                'ratio' => '3.00',
                'quote_config_id' => 3
            ],
        ]);

        $this->assertTrue($config->tva === '2.00');
        $this->assertTrue($config->pnm === '5.00');
    }
}