FROM php:7.3-apache

RUN echo "ServerName sendoshiatsu.com" >> /etc/apache2/apache2.conf

RUN apt-get update && apt-get install curl gnupg libpng-dev libmcrypt-dev zip unzip -y

RUN mkdir -p /var/www/html/gt2 && chmod -R 777 /var/www/ && chown -R www-data:www-data /var/www/
ADD --chown=www-data:www-data  . /var/www/html/gt2

WORKDIR /var/www/html/gt2

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/bin --filename=composer
RUN composer install  --no-interaction --optimize-autoloader --no-dev --prefer-dist
RUN docker-php-ext-install pdo_mysql

ENV APACHE_DOCUMENT_ROOT /var/www/html/gt2/public
RUN sed -ri -e 's!/var/www/html!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/sites-available/*.conf
RUN sed -ri -e 's!/var/www/!${APACHE_DOCUMENT_ROOT}!g' /etc/apache2/apache2.conf /etc/apache2/conf-available/*.conf
RUN a2enmod rewrite

RUN curl -sL https://deb.nodesource.com/setup_11.x | bash -
RUN apt-get update && apt-get install -y nodejs
RUN npm install && npm run production
