<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;

class ResetPassword extends Notification
{
    use Queueable;

    public $token;

    /**
     * ResetPassword constructor.
     * @param $token
     */
    public function __construct($token)
    {
        $this->token = $token;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreply@geoffreyturpin.fr', 'Geoffrey Turpin')
            ->subject('Réinitialisation du mot de passe')
            ->greeting('Hey bonsoir !')
            ->line('Tu reçois ce mail parce que tu as demandé la réinitialisation de ton mot de passe. Étonnant, non ?')
            ->action('Réinitialisation du mot de passe', url('password/reset', $this->token))
            ->line('Si la demande de provient pas de toi, tu peux t\'en foutre totalement.')
            ->line('Keur sur toi.')
            ->salutation('Cordialement, ton site internet.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
