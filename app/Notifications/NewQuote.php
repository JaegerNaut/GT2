<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use App\User;

class NewQuote extends Notification
{
    use Queueable;

    public $slug;

    /**
     * NewQuote constructor.
     * @param $slug
     */
    public function __construct($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via()
    {
        return ['mail'];
    }

    public function routeNotificationForMail()
    {
        return User::first()->email;
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->from('noreply@geoffreyturpin.fr', 'Geoffrey Turpin')
            ->subject('Un nouveau devis a été soumis')
            ->greeting('Hey bonsoir !')
            ->line('Tu reçois ce mail parce que quelqu\'un a fait une demande de devis sur le site. Incroyable.')
            ->action('Voir le devis', url('admin/devis/voir', $this->slug))
            ->line('En espérant que ça soit du lourd, du très très lourd.')
            ->line('Keur sur toi.')
            ->salutation('Cordialement, ton site internet.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
