<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specs extends Model
{

    protected $fillable = ['genre', 'minutes', 'seconds', 'description', 'image'];

    public function quote ()
    {
        return $this->belongsTo('App\Quote');
    }
}
