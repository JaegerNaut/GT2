<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuoteConfig extends Model
{
    protected $fillable = ['ratio_billing', 'tva', 'rate_picture', 'deadline', 'pnm'];
}
