<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quote extends Model
{

    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'slug',
        'status',
        'beginning_day',
        'deadline',
        'price',
        'currency',
        'email_sent',
        'session_id'
    ];

    public function specs ()
    {
        return $this->hasMany('App\Specs');
    }
}
