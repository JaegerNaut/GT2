<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $fillable = ['title', 'author', 'year', 'subtitle', 'content_fr', 'content_en', 'iframe'];
}
