<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MusicRatio extends Model
{
    protected $fillable = ['music_genre', 'ratio'];
    protected $hidden = ['id', 'created_at', 'updated_at'];
}
