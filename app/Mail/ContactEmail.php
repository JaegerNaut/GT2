<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class ContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $contact;

    /**
     * ContactEmail constructor.
     * @param $contact
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to(User::first()->email)
                    ->from('noreply@geoffreyturpin.fr', 'Geoffrey Turpin')
                    ->subject('Nouveau contact depuis le site')
                    ->view('emails.contact');
    }
}
