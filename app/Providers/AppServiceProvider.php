<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\URL;
use App\User;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('front.partials._footer', function ($view)
        {
            $view->with('user', User::first());
        });

        if(ENV('APP_ENV') === 'production') {
            URL::forceScheme('https');
        }
    }
}
