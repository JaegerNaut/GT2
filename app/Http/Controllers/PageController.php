<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Session;
use App\Http\Requests\ContactFormRequest;
use App\User;
use App\Biography;
use App\Music;
use Illuminate\Support\Facades\Mail;
use App\Mail\ContactEmail;

class PageController extends Controller
{
    public function landing ()
    {
        return view('front.landing');
    }

    public function biography ()
    {
        $bio = Biography::orderBy('date', 'DESC')->get();
        $user = User::first();
        return view('front.biography', compact('user', 'bio'));
    }

    public function music ()
    {
        $musics = Music::all();
        return view('front.music', compact('musics'));
    }

    public function contact ()
    {
        return view('front.contact');
    }

    public function send (ContactFormRequest $request)
    {
        $url = 'https://google.com/recatcha/api/siteverify';
        $data = [
            'secret' => env('RECAPTCHA_SECRET'),
            'response' => $request->get('recaptcha')
        ];

        $options = [
            'http' => [
                'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                'method' => 'POST',
                'content' => http_build_query($data)
            ]
        ];

        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        $resultJson = json_decode($result);

        if ($resultJson->success !== true) return back()->with('error', trans('pages.recaptcha_error'));

        $contact = [];
        $contact['name'] = $request->get('name');
        $contact['email'] = $request->get('email');
        $contact['subject'] = $request->get('subject');
        $contact['message'] = $request->get('message');

        Mail::to(User::first()->email)->send(new ContactEmail($contact));
        return redirect()->back()->with('status', trans('pages.sent'));
    }

    public function legal ()
    {
        return view('front.legal');
    }

    public function map ()
    {
        return view('front.map');
    }

    public function locale ($lang)
    {
        Session::put('locale', $lang);
        return redirect('/' . $lang);
    }
}
