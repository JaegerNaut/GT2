<?php

namespace App\Http\Controllers\Admin;

use App\Biography;
use App\Http\Requests\BioUpdateRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BiographyController extends Controller
{
    public function show ($id)
    {
        $bio = Biography::find($id);
        return response()->json($bio);
    }
    public function index ()
    {
        $bio = Biography::orderBy('date', 'ASC')->get();
        return view('back.biography', compact('bio'));
    }

    public function store (BioUpdateRequest $request)
    {
        Biography::create($request->all());
        return redirect()->back()->with('status', 'Date enregistrée.');
    }

    public function update (BioUpdateRequest $request)
    {
        Biography::find($request->id)->update($request->all());
        return redirect()->back()->with('status', 'Date mise à jour.');
    }

    public function destroy (Request $request)
    {
        Biography::destroy($request->id);
        return redirect()->back()->with('status', 'Année supprimée.');
    }
}
