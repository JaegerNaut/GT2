<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\QuoteSettingsUpdateRequest;
use App\QuoteConfig;
use App\MusicRatio;
use App\Quote;

class QuoteController extends Controller
{
    public function dashboard ()
    {
        $data = [
            'pending' => Quote::where('status', 'pending')->count(),
            'ongoing' => Quote::where('status', 'ongoing')->count(),
            'done' => Quote::where('status', 'done')->count(),
        ];
        $mr = MusicRatio::all();
        $qc = QuoteConfig::first();

        return view('back.dashboard', compact('data', 'mr', 'qc'));
    }

    public function quotes ($selected)
    {
        if ($selected === 'all') {
            $quotes = Quote::paginate(10);
        } else {
            $quotes = Quote::where('status', $selected)->paginate(20);
        }

        return view('back.quotes', compact('quotes'));
    }

    public function show ($slug)
    {
        $quote = Quote::where('slug', $slug)->with('specs')->first();
        return view('back.quote', compact('quote'));
    }

    public function status ($id, $status)
    {
        Quote::find($id)->update(['status' => $status]);
        return redirect()->back()->with('status', 'Le statut du projet a été mit à jour');
    }

    public function destroy ($id)
    {
        Quote::destroy($id);
        return redirect()->route('admin.quotes', ['selected' => 'all'])->with('status', 'Devis supprimée.');
    }

    public function config ()
    {
        $mr = MusicRatio::all();
        $config = QuoteConfig::first();
        return view('back.config', compact('config', 'mr'));
    }

    public function update (QuoteSettingsUpdateRequest $request)
    {
        $data = [];
        $fields = ['ratio_billing', 'tva', 'rate_picture', 'deadline', 'pnm'];
        foreach ($fields as $field) {
            $data[$field] = $request->$field;
        }
        $config = QuoteConfig::first();
        $config->update($data);
        MusicRatio::query()->delete();
        foreach ($request->music_ratio as $ratio) {
            MusicRatio::create($ratio);
        }
        return redirect()->back()->with('status', 'Information des ratios de devis enregistrées');
    }
}