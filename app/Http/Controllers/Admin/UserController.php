<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserUpdateRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\User;



class UserController extends Controller {

    public function edit ()
    {
        $user = User::first();
        return view('back.user', compact('user'));
    }

    public function update (UserUpdateRequest $request)
    {
        $data = [];

        if ($request->has('photo'))
        {
            $path = $request->file('photo')->storeAs('public/user', $request->file('photo')->getClientOriginalName());
            $photo = '/' . str_replace('public', 'storage', $path);
        }

        $fields = ['email', 'password', 'phone', 'photo', 'description_fr', 'description_en'];

        foreach($fields as $field) {
            if ($request->has($field)) {
                if ($field === 'password') {
                    $data[$field] = Hash::make($request->$field);
                } elseif ($field === 'photo') {
                    $data['picture'] = $photo;
                } else {
                    $data[$field] = $request->$field;
                }
            }
        }

        User::first()->update($data);
        return redirect()->back()->with('status', 'Information du profil enregistrées');
    }

    public function logout ()
    {
        return redirect()->route('home')->with(Auth::logout());
    }
}