<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\QuoteRequest;
use App\MusicRatio;
use App\Notifications\NewQuote;
use App\Quote;
use App\QuoteConfig;
use App\Specs;
use App\User;
use Barryvdh\DomPDF\Facade as PDF;
use DateTime;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cookie;

class FrontQuoteController extends Controller
{
    public function create ()
    {
        $types = MusicRatio::all();
        return view('back.quote_new', compact('types'));
    }

    public function store (QuoteRequest $request)
    {
        $data = [];
        $fields = ['first_name', 'last_name', 'email', 'currency', 'beginning_day', 'deadline'];

        foreach ($fields as $field) {
            if ($request->has($field)) {
                if ($field === 'deadline' || $field === 'beginning_day') {
                    $data[$field] = date('Y-m-d H:i:s', strtotime($request->$field));
                } else {
                    $data[$field] = $request->$field;
                }
            }
        }

        $data['price'] = $this->setPrice($request);
        $data['session_id'] = mt_rand(1000000000, 9999999999);
        $data['slug'] = substr($data['session_id'], 0, 10);
        $data['status'] = 'created';
        $data['email_sent'] = false;

        $quote = Quote::create($data);

        $specs = [];
        foreach ($request->specs as $spec) {
            $spec['image'] = array_key_exists('image', $spec);
            $specs[] = new Specs($spec);
        }

        $quote->specs()->saveMany($specs);
        Cookie::queue('session_id', mt_rand(1000000000, 9999999999));

        return redirect()->route('front.quote.show', ['slug' => $quote->slug])->with('status', trans('quote.created'));
    }

    public function show ($slug)
    {
        $quote = Quote::where('slug', $slug)->with('specs')->first();
        if (Cookie::get('session_id')) {
            $config = QuoteConfig::first();
            return view('back.quote_show', compact('quote', 'config'));
        } else {
            return redirect()->route('front.quote.create')->withErrors(trans('quote.cookie_error'));

        }
    }

    public function edit ($slug)
    {
        $types = MusicRatio::all();
        $quote = Quote::where('slug', $slug)->with('specs')->first();
        return view('back.quote_edit', compact('quote', 'types'));
    }

    public function update (Request $request)
    {
        $data = [];
        $fields = ['first_name', 'last_name', 'email', 'currency', 'beginning_day', 'deadline'];

        foreach ($fields as $field) {
            if ($request->has($field)) {
                if ($field === 'deadline' || $field === 'beginning_day') {
                    $data[$field] = date('Y-m-d H:i:s', strtotime($request->$field));
                } else {
                    $data[$field] = $request->$field;
                }
            }
        }

        $data['price'] = $this->setPrice($request);
        $data['session_id'] = mt_rand(1000000000, 9999999999);
        $data['slug'] = substr($data['session_id'], 0, 10);
        $data['status'] = 'created';
        $data['email_sent'] = false;

        $quote = Quote::find($request->id);
        $quote->update($data);

        $specs = [];
        foreach ($request->specs as $spec) {
            $spec['image'] = array_key_exists('image', $spec);
            $specs[] = new Specs($spec);
        }

        $quote->specs()->delete();
        $quote->specs()->saveMany($specs);

        return redirect()->route('front.quote.show', ['slug' => $quote->slug])->with('status', trans('quote.updated'));
    }

    public function send ($slug)
    {
        $quote = Quote::where('slug', $slug)->first();
        if (Cookie::get('session_id')) {
            if ($quote->status === 'created' || $quote->status === 'pending') {
                $quote->status = 'pending';
                $quote->save();
                User::first()->notify(new NewQuote($slug));
                return view('back.quote_send', compact('quote'));
            }
        } else {
            return redirect()->route('back.quote.create')->with('error', 'Vous ne pouvez plus modifier votre demande de devis ou vous n\'avez pas le droit d\'y accéder');
        }
    }

    public function pdf ($slug)
    {
        $config = QuoteConfig::first();
        $quote = Quote::where('slug', $slug)->with('specs')->first();
        $pdf = PDF::loadView('pdf.quote', compact('quote', 'config'));
        return $pdf->download('quote-'. date('YmdHis') . '.pdf');
    }

    /**
     * @param $request
     * @return array
     * @throws \Exception
     */
    private function setPrice ($request)
    {
        $config = QuoteConfig::first();
        $specs = $request->specs;
        $price = 0;
        $days = 0;
        $result = [];

        foreach ($specs as $i => $spec)
        {
            $rate_second = explode('-', $spec['genre'])[1] * (float) $config->ratio_billing;
            // Setting the initial price with the defined rate per second
            $price += (($spec['minutes'] * 60) + $spec['seconds']) * $rate_second;
            // Bonus if composed for the image
            if (in_array('image', $spec)) {
                $price *= $config->rate_picture;
            }
            // Bonus if multiple musics
            if ($i >= 1) {
                $price *= $config->pnm;
            }
        }

        // Bonus if deadline is set
        if ($request->has('deadline')) {
            $price *= (float) $config->deadline;
            // Counting days until deadline
            $begin = $request->has('beginning_day ?') ? $request->beginning_day : new DateTime('now');
            $end = new DateTime($request->deadline);
            $days = $begin->diff($end)->d;

            switch ($days) {
                // 1 year = 25% reduction
                case ($days > 365):
                    $deal = 25;
                    break;
                // 9 to 12 months = 20% reduction
                case ($days <= 365 && $days > 270):
                    $deal = 20;
                    break;
                // 6 to 9 months = 10% reduction
                case ($days <= 270 && $days > 183):
                    $deal = 10;
                    break;
                default:
                    $deal = 0;
            }

            // Applying reduction
            if ($deal > 0) {
                $price = ($price - ($price / 100) * $deal);
            }
        }

        // Calculating VAT
        $vat = ($price / 100) * $config->tva;

        $result['price'] = $price;
        $result['days'] = $days;
        $result['vat'] = $vat;
        return json_encode($result);
    }

}