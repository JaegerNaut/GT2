<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\MusicRequest;
use App\Music;
use App\Http\Controllers\Controller;

class MusicController extends Controller
{

    public function show ($id)
    {
        $music = Music::find($id);
        return response()->json($music);
    }

    public function index ()
    {
        $musics = Music::all();
        return view('back.music', compact('musics'));
    }

    public function store (MusicRequest $request)
    {
        $this->validate($request, ['year' => ['required', 'regex:/\d{4}|\d{2}\/\d{4}|\d{2}\/\d{2}\/\d{4}/']]);
        Music::create($request->all());
        return redirect()->back()->with('status', 'Musique créée');
    }

    public function update (MusicRequest $request)
    {
        $this->validate($request, ['year' => ['required', 'regex:/\d{4}|\d{2}\/\d{4}|\d{2}\/\d{2}\/\d{4}/']]);
        Music::find($request->id)->update($request->all());
        return redirect()->back()->with('status', 'Musique mise à jour');
    }

    public function destroy ($id)
    {
        Music::find($id)->destroy();
        return redirect()->back()->with('status', 'Vidéo supprimée.');
    }
}
