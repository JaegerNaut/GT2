<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class QuoteSettingsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ratio_billing' => 'regex:/\d{1,}.\d{2}/|required',
            'tva' => 'regex:/\d{1,}.\d{2}/|required',
            'rate_picture' => 'regex:/\d{1,}.\d{2}/|required',
            'deadline' => 'regex:/\d{1,}.\d{2}/|required',
            'pnm' => 'regex:/\d{1,}.\d{2}/|required'
        ];
    }
}
