<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QuoteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'string|required',
            'last_name' => 'string|required',
            'email' => 'email|required',
            'currency' => 'required',
            'quote.*.genre' => 'string|required',
            'quote.*.minutes' => 'integer|required',
            'quote.*.seconds' => 'integer|required',
            'quote.*.description' => 'required',
            'quote.*.image' => 'boolean'
        ];
    }

    public function attributes()
    {
        return [
            'first_name' => @trans('quote.first_name'),
            'last_name' => @trans('quote.last_name'),
            'quote.*.genre' => @trans('quote.genre'),
            'quote.*.minutes' => 'minutes',
            'quote.*.seconds' => @trans('quote.seconds'),
            'quote.*.description' => 'details',
        ];
    }
}
