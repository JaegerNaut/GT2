<?php

Route::get('/', function () {
    return redirect('/' . App::getLocale());
});

Route::localized(function () {
    Route::get(Lang::uri('/'), 'PageController@landing')->name('home');
    Route::get(Lang::uri('music'), 'PageController@music')->name('front.music');
    Route::get(Lang::uri('biography'), 'PageController@biography')->name('front.bio');
    Route::get(Lang::uri('contact'), 'PageController@contact')->name('front.contact');
    Route::get(Lang::uri('sitemap'), 'PageController@map')->name('sitemap');
});


Route::post('/contact', 'PageController@send')->name('front.send');
Route::get('/mentions-legales', 'PageController@legal')->name('legal');

Route::get('/locale/{lang}', 'PageController@locale');
Route::get('/bio/{id}', 'Admin\BiographyController@show');
Route::get('/music/{id}', 'Admin\MusicController@show');

Auth::routes(['register' => false]);

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function ()
{
    Route::get('/', 'Admin\QuoteController@dashboard')->name('admin.dashboard');
    Route::get('/devis/configuration', 'Admin\QuoteController@config')->name('admin.settings');
    Route::post('/devis/update', 'Admin\QuoteController@update')->name('admin.update');
    Route::get('/devis/voir/{slug}', 'Admin\QuoteController@show')->name('admin.quote');
    Route::get('/devis/delete/{id}', 'Admin\QuoteController@destroy')->name('admin.quote.delete');
    Route::get('/devis/{id}/{status}', 'Admin\QuoteController@status')->name('admin.quote.change');
    Route::get('/devis/{selected}', 'Admin\QuoteController@quotes')->name('admin.quotes');

    Route::get('quote', 'Admin\FrontQuoteController@create')->name('front.quote.create');
    Route::get('quote/{slug}', 'Admin\FrontQuoteController@show')->name('front.quote.show');
    Route::get('quote/{slug}/edit', 'Admin\FrontQuoteController@edit')->name('front.quote.edit');
    Route::get('quote/{slug}/send', 'Admin\FrontQuoteController@send')->name('front.quote.send');
    Route::get('quote/{slug}/pdf', 'Admin\FrontQuoteController@pdf')->name('front.quote.pdf');
    Route::post('quote/store', 'Admin\FrontQuoteController@store')->name('front.quote.store');
    Route::post('quote/update', 'Admin\FrontQuoteController@update')->name('front.quote.update');

    Route::get('/user/edit', 'Admin\UserController@edit')->name('admin.user');
    Route::post('/user/save', 'Admin\UserController@update')->name('admin.save');
    Route::get('/user/logout', 'Admin\UserController@logout')->name('admin.logout');

    Route::get('/biographie', 'Admin\BiographyController@index')->name('admin.bio');
    Route::post('/biographie/save', 'Admin\BiographyController@store')->name('admin.bio.save');
    Route::post('/biographie/update', 'Admin\BiographyController@update')->name('admin.bio.update');
    Route::post('/biographie/delete', 'Admin\BiographyController@destroy')->name('admin.bio.destroy');

    Route::get('/musiques', 'Admin\MusicController@index')->name('admin.music');
    Route::post('/music/save', 'Admin\MusicController@store')->name('admin.music.save');
    Route::post('/music/update', 'Admin\MusicController@update')->name('admin.music.update');
    Route::post('/music/delete', 'Admin\MusicController@destroy')->name('admin.music.destroy');

});